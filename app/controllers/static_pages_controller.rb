class StaticPagesController < ApplicationController
  def home
    @disable_header = true
  end

  def about
  end
  
  def pricing
  end
  
  def services
  end
end
