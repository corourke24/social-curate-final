class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@socialcurate.com'
  layout 'mailer'
end

